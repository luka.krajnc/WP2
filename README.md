# WP2
## Introduction
This repository is where we can share analyses done within the context of the WP2 of OptForests lead by Egbert Beuker. 

## Task leaders: 

WP2.1: Egbert Beuker

WP2.2: Luka Krajnc

WP2.3: Benjamin Brachi

WP2.4: Silvio Schüler

## accessing analysis

To contribute to the analyses, clone or fork the repository. This [page](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) explains how to do. We can also help.


The analyses are also visible on this page: [https://optforests.pages.mia.inra.fr/WP2](https://optforests.pages.mia.inra.fr/WP2)

## Climate variation among provenances

Luka extracted climate variables from climate DT for all provenances and species. 
We use these variable to select provenances from as diverse climates as possible given what we have in the common gardens. 




